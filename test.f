class Int {
    # Define a field "value" of type "N->I"
    # This is read as a "Native Pointer to an Int"
    var value: N->I

    public Int() {
        # When N->I is used to initialize a value, it is read as "Generate a Native Pointer to this Value"
        value = N->I(0)
        # would be translated to:
        # int32_t* __valueXXX = (int32_t*) malloc(sizeof(int32_t));
        # *__valueXXX = 0;
        # value = __valueXXX;
        # Conversely, if N<-I is used to initialized a value, it is read as "This is a Native Pointer to a Value"
        # value = N<-(NULL)
        # would be translated to:
        # value = (int32_t*) NULL;
    }

    public Int(nPtr: N->I) {
        value = nPtr
    }

    public function plus(other: Int) {
        # Grab our value and the other value so we can use them in a native expression
        var ourVal: N->I = value
        var otherVal: N->I = other.value
        # If we were to try and resolve them in the native initializer, we'd have to make a call to
        # "LObject_get" which is a lot "uglier"
        return Int(N->I(*ourVal + *otherVal))
    }

    public function minus(other: Int) {
        var ourVal: N->I = value
        var otherVal: N->I = other.value
        return Int(N->I(*ourVal - *otherVal))
    }

    public function compareTo(other: Int) {
        # "@" is the equivalent of Java's this keyword
        return @ - other
    }

    public function toString() {
        # Grab our value from the scope so we can access it in Native calls
        var ourVal: N->I = value
        # Then determine the length of the output string
        var length: N->I = N->I(snprintf(NULL, 0, "%d", *ourVal) + 1)
        # Now, allocate a buffer of appropriate length
        var buffer: N->C = N<-C(malloc(*length*sizeof(char)))
        # And populate it with the converted value
        N<-V(snprintf(buffer, *length, "%d", *ourVal));
        # Finally, instantiate a String with the Buffer
        return String(buffer)
    }
}

class String {
    var buffer: N->C

    public String() {
        buffer = N<-C("")
    }

    public String(nPtr: N->C) {
        buffer = nPtr
    }

    public function length() {
        # Grab the buffer from the scope so we can access it in Native calls
        var ourBuffer: N->C = buffer
        # And create an Int with the appropriate string length
        return Int(N->I(strlen(ourBuffer)))
    }
}

class LinkedList {
    var head: N->LObject

    public LinkedList() {
        head = N<-LObject(NULL);
    }

    # In the current implementation, an object cannot be a member of multiple lists
    # This is something that will get fixed later, for now this is just a proof of concept
    public function add(obj: N->LObject) {
        if (head) {
            var oldHead = head
            N<-V(LObject_set(obj, "__next", oldHead))
            head = obj
        } else {
            head = N<-LObject(obj)
            N<-V(LObject_set(obj, "__next", NULL))
        }
    }

    public function get(i: Int) {
        var next = head
        var j = 0
        while (next && j < i) {
            next = N<-LObject(LObject_get(next, "__next"))
            j = j + 1
        }
        return next
    }
}

class List<T> {
    static val DEFAULT_CAPACITY = 1

    var data: N->LArray
    var size: Int

    public List() {
        @(DEFAULT_CAPACITY)
    }

    public List(capacity: Int) {
        data = N<-LArray(LArray_alloc(capacity))
        size = capacity
    }

    public function plus(value: T) {
        var newList = List(size + 1);
        #- forEach((value, i) -> {
            newList[i] = value
        }) -#
        newList[size] = value
        return value
    }

    public function get(index: Int) : T {
        if (index >= size || index < 0) {
            return nil
        } else {
            return N<-LObject(LArray_get(data, index))
        }
    }
}

function main() {
    var myList = List<Int>();
}
