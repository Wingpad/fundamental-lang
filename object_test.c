#include <stdio.h>
#include <stdlib.h>

#include "LClass.h"

LClass kNative = {
	NULL, "LNative", NULL, 0
};

LClass* kString;
LClass* kInteger;

LObject* String_toString(LObject* self, void** argv, uint32_t argc) {
	// In reality this should be a "copy" operation
	return self;
}

LObject* String_initWithNative(LObject* self, void** argv, uint32_t argc) {
	LObject_set(self, "value", (char*)argv[0]);

	return self;
}

LObject* Integer_toString(LObject* self, void** argv, uint32_t argc) {
	// Extract parameter
	int32_t value = *(int32_t*)LObject_get(self, "value");
	// Determine the length of the string, adding 1 to represent the null terminating character
	size_t length = snprintf(NULL, 0, "%d", value) + 1;
	// And allocate an appropriately-sized buffer
	char* buffer = (char*)malloc(length * sizeof(char));
	// Then, populate the buffer with the value
	snprintf(buffer, length, "%d", value);
	// And instantiate a string 
	LObject* repr = LClass_instantiate(kString);
	// With it as its value
	LObject_invoke(repr, "initWithNative", buffer, NULL);
	// Then, return the string
	return repr;
}

LObject* Integer_initWithNative(LObject* self, void** argv, uint32_t argc) {
	LObject_set(self, "value", (int32_t*)argv[0]);

	return self;
}

LClass* allocClass(char* name, uint32_t numFields) {
	LClass* fClass = (LClass*)malloc(sizeof(LClass));
	LField** fields = (LField*)malloc(numFields * sizeof(LField*));

	uint32_t i;
	for (i = 0; i < numFields; i++) {
		fields[i] = (LField*)malloc(sizeof(LField));
	}

	fClass->name = name;
	fClass->base = NULL;
	fClass->fields = fields;
	fClass->numFields = numFields;

	return fClass;
}

LClass* generateString() {
	LClass* fString = allocClass("String", 3);

	fString->fields[0]->type = &kNative;
	fString->fields[0]->name = "value";
	fString->fields[0]->defaultValue = NULL;

	fString->fields[1]->type = &kNative;
	fString->fields[1]->name = "toString";
	fString->fields[1]->defaultValue = &String_toString;

	fString->fields[2]->type = &kNative;
	fString->fields[2]->name = "initWithNative";
	fString->fields[2]->defaultValue = &String_initWithNative;

	return fString;
}

LClass* generateInteger() {
	LClass* fString = allocClass("Integer", 3);

	fString->fields[0]->type = &kNative;
	fString->fields[0]->name = "value";
	fString->fields[0]->defaultValue = NULL;

	fString->fields[1]->type = &kNative;
	fString->fields[1]->name = "toString";
	fString->fields[1]->defaultValue = &Integer_toString;

	fString->fields[2]->type = &kNative;
	fString->fields[2]->name = "initWithNative";
	fString->fields[2]->defaultValue = &Integer_initWithNative;

	return fString;
}

int main(void) {
	kString = generateString();
	kInteger = generateInteger();
	LObject* myInteger = LClass_instantiate(kInteger);

	int32_t value = 42;
	LObject_invoke(myInteger, "initWithNative", &value, NULL);

	LObject* myString = LObject_invoke(myInteger, "toString", NULL);

	printf(LObject_get(myString, "value"));

	return 0;
}