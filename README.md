# Fundamental
Fundamental aims to be a powerful language for _doing_, one that enables developers to write simpler and smarter code. It is a multi-paradigm programming language that has object-oriented, imperative and functional programming techniques. In many regards, it can be thought of as a Kotlin/Scala-like language that targets native code.

Fundamental's Compiler (`samod` - Old English for "together, too, at the same time") is currently written in Java (with the goal to one day be self-hosting) and generates C-code. Garbage collecting will be implemented via the Boehm-Demers-Weiser conservative garbage collector and the C-code generation framework originally written for use with Fundamental is called Cenjin (C Generation Engine for Java).

Like Python, Fundamental aims to have a "batteries-included" approach by including many standard features in it's standard library (such as Unit Testing, Logging, Platform Agnostic File I/O and Mathematics libraries). A standard Reflection/Annotations library is currently in the works to bring modern Reflection features to the language.

# A "Hello, World!" Example

    import Logger;
    
    function main() {
        Logger.i('Main', 'Hello, World!');
    }
_A basic Hello World program in Fundamental that uses the Logger package to post an Info message to the log._

# A More In-Depth Example
    import Logger;
    
    function main() {
        # Fundamental uses hashtags for line comments
        
        #- And its multiline comments 
         - look like this
         -#
        
        # You can declare an integer like this:
        var i: Int = 4;
        
        # Or, using implicit typing, like this:
        var j = 4
        # Notice that semi-colons are optional (except to have multiple statements on one line)
        
        # Math uses the standard operators, which are implemented as overridable functions (a la Kotlin)
        var k = i + j # translates to i.add(j)
        
        #- Functions and Lambdas are first-class citizens in Fundamental.
         - Lambda expressions are declared as such (note that usually the return type is optional):
         -#
        var square = (i: Int) -> i * i;
        
        # You can call a lambda expression like any other function
        Logger.i('Main', square(k));
    }

# Closing Notes
This document doesn't even begin to all of Fundamental's existing features and more features are in the works! If you are interested in learning about them, consult the documentation (which does not currently exist) or see the learnxiny.f file, which (a la learnxinyminutes.com) aims to help you learn about Fundamental in Y minutes.

If you are interested in working on this project feel free to reach out to me to find out how you can help out! Also, I am  working on this for fun(damental) and, since I am busy with my other research projects, there might be long gaps between progress on this language/project.
