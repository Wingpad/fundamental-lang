package in.astralra.fundamental;

import in.astralra.fundamental.generated.FundamentalLexer;
import in.astralra.fundamental.generated.FundamentalParser;
import in.astralra.fundamental.type.*;
import org.antlr.v4.runtime.ANTLRFileStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.TokenStream;
import org.antlr.v4.runtime.tree.ParseTree;

import java.io.IOException;

/**
 * Created by jszaday on 7/25/2016.
 */
public class Main {
    public static void main(String... args) throws IOException {
//        RainScope global = new RainScope(null);
//        RainClass integer = new RainClass(null, "Integer");
//        RainTypeReference reference = global.referenceType("Integer");
//        RainDeclarationImpl parameter = new RainDeclarationImpl(reference, "other");
//
//        global.declare(integer);
//
//        integer.declare(new RainFunction(global, reference, "add", parameter));
//
//        RainFuncReference funcReference = ((RainClass) reference.resolve()).referenceFunc(reference, "add", parameter);
//
//        RainFunction function = funcReference.resolve();
//
//        System.out.println(function.getName());


        ANTLRFileStream fileStream = new ANTLRFileStream(args[0]);
        FundamentalLexer lexer = new FundamentalLexer(fileStream);
        TokenStream tokenStream = new CommonTokenStream(lexer);
        FundamentalParser parser = new FundamentalParser(tokenStream);

        ParseTree parseTree = parser.program();

        FundamentalCompiler compiler = new FundamentalCompiler();
        RainElement element = compiler.visit(parseTree);
        FundamentalWriter writer = new FundamentalWriter((RainBlock) element);

        System.out.println(writer.visit());
    }
}
