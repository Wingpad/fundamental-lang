package in.astralra.fundamental.type;

import java.util.Arrays;
import java.util.List;
import java.util.logging.Logger;

/**
 * Created by jszaday on 7/28/2016.
 */
public class RainConnector extends RainExpression implements RainReference<RainTyped> {

    private RainExpression instance;
    private String target;
    private RainTyped resolved;

    private static final Logger LOGGER = Logger.getLogger(RainConnector.class.getSimpleName());

    public RainConnector(RainExpression instance, String target) {
        super(null);

        this.instance = instance;
        this.target = target;
    }

    public RainExpression getInstance() {
        return instance;
    }

    @Override
    public RainType getType() {
        return resolve().getType();
    }

    @Override
    public RainTyped resolve() {
        if (resolved != null) {
            return resolved;
        } else if (instance instanceof RainReferenceImpl) {
            RainDeclaration declaration = ((RainReferenceImpl) instance).resolve();

            if (declaration instanceof RainDeclarationImpl) {
                if (declaration.getType() instanceof RainTypeReference) {
                    declaration = (RainDeclaration) ((RainTypeReference) declaration.getType()).resolve();
                } else {
                    declaration = (RainDeclaration) declaration.getType();
                }
            }

            if (declaration instanceof RainClass) {
                List<RainDeclaration> declarationList = ((RainClass) declaration).resolve(target);

                if (declarationList.isEmpty()) {
                    throw new RuntimeException("Unable to resolve " + target + "!");
                } else if (declarationList.get(0) instanceof RainFunction) {
                    return (resolved = new RainMultiReference(declarationList));
                } else {
                    return (resolved = new RainReferenceImpl(target, declarationList.get(0)));
                }
            } else {
                throw new RuntimeException("Not sure what to do with an class of type " + instance.getClass().getSimpleName());
            }
        } else if (instance instanceof RainFunctionCall) {
            if (instance.getType() instanceof RainClass) {
                List<RainDeclaration> results = ((RainClass) instance.getType()).resolve(target);

                if (results.isEmpty()) {
                    throw new RuntimeException("Cannot resolve " + target + " within " + instance.getType().getName());
                }

                // target = ((RainFunctionCall) instance).findMatchingIn(results).getExternalName();
            } else {
                throw new RuntimeException("Not sure what to do with an instance of type" + instance.getType());
            }

            return (resolved = instance);
        } else {
            throw new RuntimeException("Not sure what to do with an instance of type " + instance.getClass().getSimpleName());
        }
    }

    @Override
    public String toString() {
        resolve();

        if (resolved instanceof RainFunctionCall) {
            return "LObject_get(" + instance + ", \"" + target + "\")";
        } if (resolved instanceof RainReferenceImpl) {
            return "LObject_get(" + instance + ", \"" + ((RainReferenceImpl) resolved).getName() + "\")";
        } else {
            LOGGER.warning("Rain connector referred to directly, this could be an error. Stack Trace: " + Arrays.toString(Thread.currentThread().getStackTrace()));
            return target;
        }
    }
}
