package in.astralra.fundamental.type;

import in.astralra.cenjin.core.Element;

/**
 * Created by jszaday on 7/25/2016.
 */
public interface RainReference<T> {
    T resolve();
}
