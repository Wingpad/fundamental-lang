package in.astralra.fundamental.type;

/**
 * Created by jszaday on 7/29/2016.
 */
public class RainWhile implements RainElement {
    private RainExpression expression;
    private RainElement action;

    public RainWhile(RainExpression expression, RainElement action) {
        this.expression = expression;
        this.action = action;
    }

    public RainElement getAction() {
        return action;
    }

    public RainExpression getExpression() {
        return expression;
    }

    @Override
    public boolean needsSemicolon() {
        return action.needsSemicolon();
    }

    @Override
    public String toString() {
        return "while (" + expression + ") " + action;
    }
}
