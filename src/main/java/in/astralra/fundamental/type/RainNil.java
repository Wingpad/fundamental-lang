package in.astralra.fundamental.type;

/**
 * Created by jszaday on 8/1/16.
 */
public enum RainNil implements RainType {

    // This probably won't be used outside of this class but I'm just exposing it in case.
    NIL; // , UNDEFINED;
    // Undefined could easily be added as such

    private final RainDeclaration declaration;

    RainNil() {
        declaration = new RainDeclarationImpl(this, name().toLowerCase());
    }

//    RainNil(String name) {
//        declaration = new RainDeclarationImpl(this, name);
//    }

    public RainDeclaration getDeclaration() {
        return declaration;
    }

    @Override
    public String getName() {
        return name();
    }

    @Override
    public String getIdentifier() {
        return RainNativeType.VOID.getIdentifier();
    }

    @Override
    public boolean isAssignableFrom(RainType type) {
        // TODO Implement NONE behavior across the board - TBD if this is needed.
        return true;
    }

    @Override
    public boolean needsSemicolon() {
        throw new RuntimeException("this shouldn't be called on its own.");
    }
}
