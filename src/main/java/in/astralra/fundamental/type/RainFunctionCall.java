package in.astralra.fundamental.type;

import com.sun.xml.internal.bind.v2.model.annotation.RuntimeInlineAnnotationReader;

import java.security.cert.PKIXRevocationChecker;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Created by jszaday on 7/26/2016.
 */
public class RainFunctionCall extends RainExpression implements RainReference<RainFunction> {
    private RainScope scope;
    private RainMultiReference target;
    private List<RainExpression> arguments;
    private RainFunction resolved;

    public RainFunctionCall(RainScope scope, RainExpression target, List<RainExpression> arguments) {
        super(null);

        this.scope = scope;
        this.target = new RainMultiReference(scope, target);
        this.arguments = arguments;
    }

    @Override
    public RainFunction resolve() {
        if (resolved == null) {
            resolved = findMatchingIn(target.resolve());
        }

        return resolved;
    }

    @Override
    public RainType getType() {
        return resolve().getReturnType();
    }

    public RainFunction findMatchingIn(List<? extends RainDeclaration> list) {
        List<RainType> types = arguments.stream().map(RainExpression::getType).collect(Collectors.toList());
        // TODO throw error if non-functions are matched
        List<RainFunction> results = list.stream()
                .filter(declaration -> declaration instanceof RainFunction)
                .map(RainFunction.class::cast)
                .filter(function -> function.argumentsMatch(types))
                .collect(Collectors.toList());

        if (results.isEmpty() || results.size() > 1) {
            throw new RuntimeException("Could not resolve call to " + target + " !");
        } else {
            return results.get(0);
        }
    }

    @Override
    public String toString() {
        // Resolve this "reference"
        resolve();

        String name = resolved.getExternalName();

        String self;
        if (target.isConstructor()) {
            self = new RainInstance(target.whichClass()).toString();
        } else {
            Object tTarget = target.getTarget();

            if (tTarget instanceof RainConnector) {
                self = String.valueOf(((RainConnector) tTarget).getInstance());
            } else if (scope.isField(resolved)) {
                self = "self";
            } else {
                throw new RuntimeException("What should self be??");
            }
        }

        String args = arguments.stream().map(String::valueOf).collect(Collectors.joining(", "));

        if (args.isEmpty()) {
            args = "NULL";
        } else {
            args += ", NULL";
        }

        return String.format("LObject_invoke(%s, \"%s\", %s)", self, name, args);
    }
}
