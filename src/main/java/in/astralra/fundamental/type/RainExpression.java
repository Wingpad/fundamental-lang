package in.astralra.fundamental.type;

/**
 * Created by jszaday on 7/25/2016.
 */
public class RainExpression implements RainElement, RainTyped {
    private RainType type;

    public RainExpression(RainType type) {
        this.type = type;
    }

    @Override
    public RainType getType() {
        return type;
    }

    @Override
    public boolean needsSemicolon() {
        return true;
    }
}
