package in.astralra.fundamental.type;

/**
 * Created by jszaday on 7/27/2016.
 */
public class RainReturn implements RainElement {

    private RainExpression value;

    public RainReturn(RainExpression value) {
        this.value = value;
    }

    public RainExpression getValue() {
        return value;
    }

    @Override
    public String toString() {
        if (value == null) {
            return "return";
        } else {
            return "return " + value;
        }
    }

    @Override
    public boolean needsSemicolon() {
        return true;
    }
}
