package in.astralra.fundamental.type;

/**
 * Created by jszaday on 7/28/2016.
 */
public interface RainTyped {
    RainType getType();
}
