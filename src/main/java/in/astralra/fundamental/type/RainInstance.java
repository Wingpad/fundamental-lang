package in.astralra.fundamental.type;

import java.util.Optional;

/**
 * Created by jszaday on 8/2/2016.
 */
public class RainInstance extends RainExpression {

    private RainExpression instantiation;

    public RainInstance(RainClass type, RainType... typeParameters) {
        super(type);
        this.instantiation = new RainNativeExpression(RainNativeType.OBJECT, "LClass_instantiate(k", type.getName(), ")");
    }

    @Override
    public String toString() {
        return String.valueOf(instantiation);
    }
}
