package in.astralra.fundamental.type;

import java.util.Arrays;
import java.util.Optional;

/**
 * Created by jszaday on 7/25/2016.
 */
public enum RainNativeType implements RainType {
    VOID("V", "void*"), INT("I", "int32_t*", "Int"),
    STRING("C", "char*", "String"), CLASS("LClass", "LClass*"),
    FUNCTION("LFunction", "LFunction"), SHORT("S", "int16_t*"),
    BYTE("B", "int8_t*"), BOOLEAN("Z", "bool*"), FLOAT("F", "float*"),
    DOUBLE("D", "double*"), LONG("J", "int64_t*"), OBJECT("LObject", "LObject*"),
    ARRAY("LArray", "LArray*");

    private final String name;
    private final String identifier;
    private final String wrapper;

    RainNativeType(String name, String identifier) {
        this(name, identifier, null);
    }

    RainNativeType(String name, String identifier, String wrapper) {
        this.name = name;
        this.identifier = identifier;
        this.wrapper = wrapper;
    }

    public String getWrapper() {
        return wrapper;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getIdentifier() {
        return identifier;
    }

    public static Optional<RainNativeType> lookup(String name) {
        return Arrays.stream(values())
                .filter(value -> value.getName().equals(name))
                .findFirst();
    }

    @Override
    public boolean needsSemicolon() {
        throw new RuntimeException("A type cannot be in the main scope.");
    }

    @Override
    public boolean isAssignableFrom(RainType type) {
        if (type instanceof RainTypeReference) {
            type = ((RainTypeReference) type).resolve();
        }

        return this == OBJECT && type instanceof RainClass || type == VOID || this == type;
    }
}
