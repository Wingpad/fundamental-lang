package in.astralra.fundamental.type;

import java.util.Optional;

/**
 * Created by jszaday on 7/25/2016.
 */
public interface RainDeclaration extends RainElement, RainTyped {
    String getName();
    Optional<RainElement> getValue();
    int getFlags();
    void setFlags(int integer);
}
