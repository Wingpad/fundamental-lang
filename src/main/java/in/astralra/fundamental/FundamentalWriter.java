package in.astralra.fundamental;

import in.astralra.cenjin.core.Declaration;
import in.astralra.fundamental.type.*;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

/**
 * Created by jszaday on 7/27/2016.
 */
public class FundamentalWriter {

    // Add separation of LFunction/LMember
    private final static String LFUNCTION_SIGNATURE = "(LObject* self, void** argv, uint32_t argc)";
    private final String tab;
    private RainBlock main;
    private LinkedList<RainClass> classStack;
    private String prototypes = "";

    public FundamentalWriter(RainBlock main, String tab) {
        this.main = main;
        this.tab = tab;
        this.classStack = new LinkedList<>();
    }

    public FundamentalWriter(RainBlock main) {
        this(main, "\t");
    }

    public String visit() {
        String stuff = visit(main);
        return prototypes + stuff;
    }

    private String visit(RainElement element) {
        if (element instanceof RainBlock) {
            return visitBlock((RainBlock) element);
        } else if (element instanceof RainClass) {
            return visitClass((RainClass) element);
        } else if (element instanceof RainIf) {
            return visitIf((RainIf) element);
        } else if (element instanceof RainWhile) {
            return visitWhile((RainWhile) element);
        } else {
            return String.valueOf(element);
        }
    }

    public String visitClass(RainClass rClass) {
        StringBuilder builder = new StringBuilder();
        List<RainDeclaration> declarations = rClass.getDeclarations();

        classStack.push(rClass);

        declarations.forEach(declaration -> {
            if (declaration instanceof RainFunction) {
                builder.append(visitFunction((RainFunction) declaration)).append(System.lineSeparator());
            }
        });

        prototypes += "LClass* k" + rClass.getName() + ";" + System.lineSeparator();
        String generator = "LClass* generate" + rClass.getName() + "() {" + System.lineSeparator();
        generator += tab + "LClass* lClass = LClass_alloc(\"" + rClass.getName() + "\", " + declarations.size() + ");" + System.lineSeparator();
        for (int i = 0; i < declarations.size(); i++) {
            RainDeclaration declaration = declarations.get(i);
            String name, value;
            if (declaration instanceof RainFunction) {
                name = ((RainFunction) declaration).getExternalName();
                value = "&" + rClass.getName() + "_" + name;
            } else {
                name = declaration.getName();
                value = "NULL";
            }
            // TODO Fix this
            generator += tab + "lClass->fields["+i+"]->type = NULL;" + System.lineSeparator();
            generator += tab + "lClass->fields["+i+"]->name = \""+name+"\";" + System.lineSeparator();
            generator += tab + "lClass->fields["+i+"]->defaultValue = "+value+";" + System.lineSeparator();
        }
        generator += tab + "return lClass;" + System.lineSeparator() + "}";

        builder.append(generator);

        classStack.pop();

        return builder.toString();
    }

    public String visitFunction(RainFunction function) {
        StringBuilder builder = new StringBuilder();
        RainType optional = function.getReturnType();
        RainType rainType;

        if (optional instanceof RainNativeType && optional != RainNativeType.OBJECT) {
            throw new RuntimeException("Cannot return a value of type " + optional);
        } else {
            rainType = RainNativeType.OBJECT;
        }

        builder.append(rainType.getIdentifier());

        String name = function.getExternalName();
        if (!classStack.isEmpty() && classStack.peek().isField(function)) {
            name = classStack.peek().getName() + "_" + name;
        }

        builder.append(" ").append(name).append(LFUNCTION_SIGNATURE).append(" ");
        builder.append(visitBlock(function));

//        if (function.getReturnType() == RainVoid.getInstance()) {
//            builder.append("return NULL;");
//        }

        return builder.toString();
    }

    public String visitBlock(RainBlock block) {
        boolean writeBrackets = block != main;
        StringBuilder builder = new StringBuilder();

        if (writeBrackets) {
            builder.append("{").append(System.lineSeparator());
        }

        block.listElements().forEach(element -> {
            if (writeBrackets) {
                builder.append(tab);
            }


            builder.append(visit(element));

            if (element.needsSemicolon()) {
                builder.append(";");
            }

            builder.append(System.lineSeparator());
        });

        if (writeBrackets) {
            builder.append("}");
        }

        return builder.toString();
    }

    public String visitIf(RainIf rainIf) {
        StringBuilder builder = new StringBuilder();

        builder.append("if (").append(visit(rainIf.getExpr())).append(") ").append(visit(rainIf.getIfCase()));

        if (rainIf.getIfCase().needsSemicolon()) {
            builder.append(";");
        }

        if (rainIf.getElseCase() != null) {
            builder.append(" else ").append(visit(rainIf.getElseCase()));

            if (rainIf.getElseCase().needsSemicolon()) {
                builder.append(";");
            }
        }

        return builder.toString();
    }

    public String visitWhile(RainWhile rainWhile) {
        StringBuilder builder = new StringBuilder();

        builder.append("while (").append(visit(rainWhile.getExpression())).append(") ").append(visit(rainWhile.getAction()));

        if (rainWhile.needsSemicolon()) {
            builder.append(";");
        }

        return builder.toString();
    }
}
