package in.astralra.fundamental;

import in.astralra.fundamental.generated.FundamentalBaseVisitor;
import in.astralra.fundamental.generated.FundamentalParser;
import in.astralra.fundamental.type.*;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by jszaday on 7/25/2016.
 */
public class FundamentalCompiler extends FundamentalBaseVisitor<RainElement> {

    private RainBlock global;
    private RainBlock currScope;
    private LinkedList<RainClass> classStack;

    private static String removeLastCharacter(String str) {
        return str.substring(0, str.length() - 1);
    }

    @Override
    public RainElement visitProgram(FundamentalParser.ProgramContext ctx) {
        global = new RainBlock(null);
        currScope = global;
        classStack = new LinkedList<>();

        Arrays.stream(RainNil.values())
                .map(RainNil::getDeclaration)
                .forEach(currScope::declare);

        super.visitProgram(ctx);

        return global;
    }

    @Override
    public RainElement visitStatement(FundamentalParser.StatementContext ctx) {
        RainElement element = visit(ctx.getChild(0));

        if (element != null) {
            currScope.add(element);
        }

        return element;
    }

    @Override
    public RainElement visitClassDefinition(FundamentalParser.ClassDefinitionContext ctx) {
        // TODO Add extends as field so we can grab interfaces and parents
        // Push a new class onto the stack
        classStack.push(new RainClass(null, ctx.Id().get(0).getText()));
        // Declare the current class within the scope
        currScope.declare(classStack.peek());
        // And create a new context with the current class as the self
        currScope = new RainBlock(currScope, classStack.peek());
        // Add the type generics to the class TODO this still needs work
        FundamentalParser.TypeListContext listContext = ctx.typeParameters().isEmpty() ? null : ctx.typeParameters().get(0).typeList();
        while (listContext != null) {
            RainType typeParameter = new RainTypeParameter(listContext.boundedType().getText());
            classStack.peek().addType(typeParameter);
            currScope.addType(typeParameter);
            listContext = listContext.typeList();
        }
        // Visit all of the declarations
        super.visitClassDefinition(ctx);
        // Leave the temporary context
        currScope = (RainBlock) currScope.leave();
        // Then return the populated object
        return classStack.pop();
    }

    @Override
    public RainElement visitBlock(FundamentalParser.BlockContext ctx) {
        RainBlock newBlock = new RainBlock(currScope, classStack.peek());
        currScope = newBlock;
        ctx.statement().forEach(this::visitStatement);
        currScope = (RainBlock) newBlock.leave();
        return newBlock;
    }

    @Override
    public RainElement visitClassDeclaration(FundamentalParser.ClassDeclarationContext ctx) {
        RainDeclaration declaration;

        int flags = 0;
        for (FundamentalParser.DeclarationModifierContext context : ctx.declarationModifier()) {
            flags |= ((RainAccessModifier) visitDeclarationModifier(context)).getFlag();
        }

        if (ctx.constructor() == null) {
            declaration = (RainDeclaration) visitDeclaration(ctx.declaration());
        } else {
            declaration = (RainDeclaration) visitConstructor(ctx.constructor());
        }

        // Declare the function within the current class with the appropriate flags
        classStack.peek().declare(declaration, flags);

        return declaration;
    }

    @Override
    public RainElement visitDeclarationModifier(FundamentalParser.DeclarationModifierContext ctx) {
        return RainAccessModifier.lookup(ctx.getText()).get();
    }

    @Override
    public RainElement visitConstructor(FundamentalParser.ConstructorContext ctx) {
        RainFunction constuctor = new RainFunction(currScope, classStack.peek(), "init", visitFunctionArgs(ctx.functionArgumentList()));

        currScope.declare(constuctor);

        currScope = constuctor;

        Arrays.stream(constuctor.getArguments()).forEach(currScope::add);

        ctx.block().statement().forEach(this::visitStatement);

        currScope.add(new RainReturn(new RainNativeExpression(classStack.peek(), "self")));

        currScope = (RainBlock) constuctor.leave();

        return constuctor;
    }

    // TODO Declaration needs to add itself to the list before adding all of its children
    @Override
    public RainElement visitDeclaration(FundamentalParser.DeclarationContext ctx) {
        String name = ctx.Id().getText();
        if (ctx.block() == null) {
            RainType type;
            RainExpression expression;

            if (ctx.expression() == null) {
                expression = null;
            } else {
                expression = (RainExpression) visitExpression(ctx.expression());
            }

            // TODO Validate Expr. Type = Decl. Type
            if (ctx.type() != null) {
                type = (RainType) visitType(ctx.type());
            } else if (expression != null) {
                type = expression.getType();
            } else {
                throw new RuntimeException("Could not derive the type of declaration for " + name);
            }

            RainDeclaration result = new RainDeclarationImpl(type, name, expression);

            currScope.declare(result);

            return result;
        } else {
            RainType type = ctx.type() == null ? null : (RainType) visitType(ctx.type());
            Integer flags = ctx.Val() == null ? 0 : RainAccessModifier.FINAL.getFlag();

            RainFunction result = new RainFunction(currScope, type, name, visitFunctionArgs(ctx.functionArgumentList()));

            currScope.declare(result, flags);

            currScope = result;

            Arrays.stream(result.getArguments()).forEach(currScope::add);

            ctx.block().statement().forEach(this::visitStatement);

            currScope = (RainBlock) result.leave();

            return result;
        }
    }

    private RainDeclarationImpl[] visitFunctionArgs(FundamentalParser.FunctionArgumentListContext context) {
        List<RainDeclarationImpl> variables = new ArrayList<>();

        while (context != null) {
            RainType varType = context.type() == null ? null : (RainType) visitType(context.type());
            // TODO: 7/25/2016 Change null to actual value when defaults get added?? IDK.
            variables.add(new RainDeclarationImpl(varType, context.Id().getText(), null));
            context = context.functionArgumentList();
        }

        Collections.reverse(variables);

        return variables.toArray(new RainDeclarationImpl[variables.size()]);
    }

    @Override
    public RainElement visitPrimaryExpression(FundamentalParser.PrimaryExpressionContext ctx) {
        if (ctx.Id() != null) {
            return new RainReferenceImpl(currScope, ctx.Id().getText());
        } else if (ctx.NativeValue() != null) {
            Matcher matcher = (Pattern.compile("N(<-|->)(.*?)\\((.*)\\)")).matcher(ctx.getText());
            if (matcher.find()) {
                boolean isPointer = matcher.group(1).equals("<-");
                RainNativeType type = RainNativeType.lookup(matcher.group(2).trim()).get();
                return visitCompoundElement(new RainNativeValue(currScope, type, matcher.group(3).trim(), isPointer));
            } else {
                throw new RuntimeException("Could not parse native expression: " + ctx.getText());
            }
        } else if (ctx.Constant() != null) {
            RainExpression expression = (RainExpression) visitCompoundElement(new RainNativeValue(currScope, RainNativeType.INT, ctx.Constant().getText()));
            return new RainFunctionCall(
                    currScope, new RainReferenceImpl(currScope, RainNativeType.INT.getWrapper()), Collections.singletonList(expression)
            );
        } else {
            return null;
        }
    }

    private RainElement visitCompoundElement(RainCompoundElement compoundElement) {
        RainElement[] elements = compoundElement.getElements();
        Spliterator<RainElement> iterator = Arrays.spliterator(elements, 0, elements.length - 1);
        iterator.forEachRemaining(currScope::add);
        return elements[elements.length - 1];
    }

    @Override
    public RainElement visitPostfixExpression(FundamentalParser.PostfixExpressionContext ctx) {
        if (ctx.primaryExpression() != null) {
            return visitPrimaryExpression(ctx.primaryExpression());
        } else if (ctx.LParen() != null) {
            RainExpression target = (RainExpression) visitPostfixExpression(ctx.postfixExpression());
            List<RainExpression> arguments = new ArrayList<>();
            FundamentalParser.ArgumentExpressionListContext listContext = ctx.argumentExpressionList();
            while (listContext != null) {
                arguments.add((RainExpression) visit(listContext.conditionalExpression()));
                listContext = listContext.argumentExpressionList();
            }
            return new RainFunctionCall(currScope, target, arguments);
        } else if (ctx.Id() != null) {
            RainExpression target = (RainExpression) visitPostfixExpression(ctx.postfixExpression());
            return new RainConnector(target, ctx.Id().getText());
        } else {
            RainExpression target = (RainExpression) visitPostfixExpression(ctx.postfixExpression());
            RainExpression index = (RainExpression) visitExpression(ctx.expression());
            RainConnector connector = new RainConnector(target, "get");
            return new RainFunctionCall(currScope, connector, Collections.singletonList(index));
        }
    }

    @Override
    public RainElement visitType(FundamentalParser.TypeContext ctx) {
        String id;
        if (ctx.NativeType() == null) {
            id = ctx.Id().getText();
            // Reference the type in the scope
            return currScope.referenceType(id);
        } else {
            // Drop the N->I prefix
            id = ctx.NativeType().getText().substring(3);
            // And lookup the Id, not checking the Optional so it throws an error if its missing
            return RainNativeType.lookup(id).get();
        }
    }

    @Override
    public RainElement visitAssignment(FundamentalParser.AssignmentContext ctx) {
        RainOperator operator = RainOperator.lookup(
                removeLastCharacter(ctx.assignmentOperator().getText())
        ).get();

        RainExpression expression = (RainExpression) visitPostfixExpression(ctx.postfixExpression());
        RainExpression value = (RainExpression) visitConditionalExpression(ctx.conditionalExpression());

        return new RainAssignment(expression, operator, value);
    }

    @Override
    public RainElement visitAdditiveExpression(FundamentalParser.AdditiveExpressionContext ctx) {
        RainExpression rhs = (RainExpression) visitMultiplicativeExpression(ctx.multiplicativeExpression());

        if (ctx.additiveExpression() != null) {
            RainExpression lhs = (RainExpression) visit(ctx.additiveExpression());
            RainOperator operator = ctx.Plus() == null ? RainOperator.SUBTRACT : RainOperator.ADD;
            return new RainFunctionCall(currScope, new RainConnector(lhs, operator.getFunction()), Collections.singletonList(rhs));
        } else {
            return rhs;
        }
    }

    @Override
    public RainElement visitReturnStatement(FundamentalParser.ReturnStatementContext ctx) {
        return new RainReturn((RainExpression) visitExpression(ctx.expression()));
    }

    @Override
    public RainElement visitIfStatement(FundamentalParser.IfStatementContext ctx) {
        RainElement rainElement = ctx.statement() == null ? visitBlock(ctx.block()) : visitStatement(ctx.statement());
        RainExpression expression = (RainExpression) visitExpression(ctx.expression());

        if (ctx.elseStatement() == null) {
            return new RainIf(expression, rainElement);
        } else {
            return new RainIf(expression, rainElement, visitElseStatement(ctx.elseStatement()));
        }
    }

    @Override
    public RainElement visitWhileStatement(FundamentalParser.WhileStatementContext ctx) {
        RainElement rainElement = ctx.statement() == null ? visitBlock(ctx.block()) : visitStatement(ctx.statement());
        RainExpression expression = (RainExpression) visitExpression(ctx.expression());
        return new RainWhile(expression, rainElement);
    }

    @Override
    public RainElement visitRelationalExpression(FundamentalParser.RelationalExpressionContext ctx) {
        RainExpression shiftExpression = (RainExpression) visitShiftExpression(ctx.shiftExpression());
        if (ctx.relationalExpression() == null) {
            return shiftExpression;
        } else {
            RainExpression relational = (RainExpression) visitRelationalExpression(ctx.relationalExpression());
            RainFunctionCall compareTo = new RainFunctionCall(currScope, new RainConnector(relational, "compareTo"), Collections.singletonList(shiftExpression));
            RainConnector getValue = new RainConnector(compareTo, "value");
            RainOperator operator;

            if (ctx.LessThan() != null) {
                operator = RainOperator.LESS_THAN;
            } else if (ctx.GreaterThan() != null) {
                operator = RainOperator.GREATER_THAN;
            } else if (ctx.LessThanEquals() != null) {
                operator = RainOperator.LESS_THAN_EQUALS;
            } else if (ctx.GreaterThanEquals() != null) {
                operator = RainOperator.GREATER_THAN_EQUALS;
            } else {
                throw new RuntimeException("Operator unrecognized, " + ctx.getText());
            }

            return new RainNativeExpression(RainNativeType.BOOLEAN,
                    // Lazily build the expression in case things haven't been resolved yet
                    "(*(", RainNativeType.INT.getIdentifier(), ")", getValue, ") ", operator.getOperator(), " 0"
            );
        }
    }
}
