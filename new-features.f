import Console
import Collections.Map

# Case Classes
case class Animal(species: String);

# Handling Constructor Parameters When Extending a Class
case class Dog(name: String, bark: Function<String>) extends Animal("Canis lupus familiaris");

case class Vegetable(type: String)

class VegetableInventory {
    private inventory: Map<Vegetable, Int>
    
    public VegetableInventory() {
        # Note that XYZ has a diamond operator just like Java
        inventory = Map<>()
    }
    
    public function howMany(vege: Vegetable) {
        # Note the use of the Elvis operator for null-safety
        return inventory[vege] ?: 0
    }

    public function add(vege: Vegetable) {
        inventory[vege] = howMany(vege) + 1
    }
    
    public function add(vege: Vegetable, quantity: Int) {
        inventory[vege] = howMany(vege) + quantity
    }
}

function main() {
    var myVeges = VegetableInventory()
    var carrot = Vegetable("Carrot")
    
    # Case classes are instantiated just like any other
    var fido = Dog("Fido", () -> "bark!!");

    # String Substitutions
    Console.writeLn("Main", "I'm {fido.name} and I can {fido.bark()}")

    # Pipe Operator (un-overridable)
    fido.species |> String::plus.apply(" is my species name.") |> Console.writeLn
    # Produces "Canis lupus familiaris is my species name."
    
    # Trying to select a function with multiple definitions will cause a compilation error
    # VegetableInventory::add.apply(myVeges, carrot)
    # -> Cannot resolve VegetableInventory::add, multiple implementations detected
    
    # As such, you have to select which version you want to retrieve
    VegetableInventory::add<Void, Vegetable, Int>.apply(myVeges, carrot, 4)
    # Where this is read as ReturnType, Args...
    
    # Why do you have to use :: to reference a function within a class? Well...
    # VegetableInventory.add
    # Will fail with: "Cannot refer to 'add' in a static context"
    # In other words, using :: lets the compiler know that the method will accept a self parameter.
    
    Console.writeLn(myVeges.howMany(carrot)) # -> 4
}
