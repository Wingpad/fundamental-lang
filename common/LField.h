#ifndef FUNDAMENTAL_LFIELD_H
#define FUNDAMENTAL_LFIELD_H

#include "LClass.h"

// Forward Declare Class
struct LClass;

typedef struct LField {
  struct LClass* type;
  char* name;
  void* defaultValue;
} LField;

#endif
