#ifndef FUNDAMENTAL_LCLASS_H
#define FUNDAMENTAL_LCLASS_H

// For Object
#include "LObject.h"

// For Field
#include "LField.h"

// Forward declare Object
struct LClass;

// Forward declare Field
struct LField;

typedef struct LClass {
  struct LClass* base;
  char* name;
  LField** fields;
  uint32_t numFields;
} LClass;

struct LObject* LClass_instantiate(LClass* lClass);
struct LClass* LClass_alloc(char* name, uint32_t numFields);

#endif
