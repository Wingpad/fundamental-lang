#include "LObject.h"

// For strlen
#include <string.h>
// For malloc
#include <stdlib.h>
// For assert
#include <assert.h>
// For vargs
#include <stdarg.h>

// Typedef for valid Fundamental Functions
typedef LObject* (*LFunction)(LObject* self, void** argv, uint32_t argc);

/**
 * Simple Bob Jenkins's hash algorithm taken from the
 * wikipedia description.
 */
static uint32_t default_hash(char *key)
{
    size_t len = strlen(key);
    uint32_t hash = 0;
    uint32_t i = 0;

    for(hash = i = 0; i < len; ++i)
    {
        hash += key[i];
        hash += (hash << 10);
        hash ^= (hash >> 6);
    }

    hash += (hash << 3);
    hash ^= (hash >> 11);
    hash += (hash << 15);

    return hash;
}

LObjectNode* LObject_findField(LObject* self, char* name, uint32_t* hash) {
  LObjectNode* curr;

  // If no hash value was provided
  if (*hash == 0) {
    // Initialize it
    *hash = default_hash(name);
  }

  // Then, for each of the nodes
  for (curr = self->first; curr; curr = curr->next) {
    // If a node matches
    if (curr->hash == *hash) {
      // Return it
      return curr;
    }
  }

  // Otherwise, if no match was found and a superclass exists
  if (self->base) {
    // Search it as well
    return LObject_findField(self->base, name, hash);
  } else {
    // Otherwise, just return NULL to indicate a match was not found
    return NULL;
  }
}

void LObject_set(LObject* self, char* name, void* value) {
    uint32_t hash = 0;
    // Try and locate the field using the field finder
    LObjectNode* found = LObject_findField(self, name, &hash);

    // If the node was found
    if (found) {
      // Update its value, otherwise...
      found->value   = value;
    } else {
      // Initialize a new node w/ the hash and value pointing to the next in the sequence
      LObjectNode* newNode  = malloc(sizeof(LObjectNode));
      newNode->next  = self->first;
      newNode->hash  = hash;
      newNode->value = value;
	  newNode->finalized = NULL;
      // And set it as the first node
      self->first = newNode;
    }
}

void* LObject_get(LObject* self, char* name) {
  uint32_t hash = 0;
  // Try and locate the field using the field finder
  LObjectNode* found = LObject_findField(self, name, &hash);

  // If the node was found
  if (found) {
    // Update its value, otherwise...
    return found->value;
  } else {
    // Otherwise, return NULL
    return NULL;
  }
}

// TODO Make this an O(n) operation for arguments
LObject* LObject_invoke(LObject* self, char* name, ...) {
	uint32_t i;
	va_list args1, args2;

	// grab the function and assert it's valid
	LFunction function = (LFunction)LObject_get(self, name);
	assert(function);

	va_start(args1, name);
	va_copy(args2, args1);

	uint32_t argc = 0;
	while (va_arg(args1, void*)) argc++;

	va_end(args1);

	if (argc) {
		// Allocate a new vector
		void** argv = malloc(argc * sizeof(void*));
		// And copy the arguments
		for (i = 0; i < argc; i++) {
			argv[i] = va_arg(args2, void*);
		}
		// End the varargs
		va_end(args2);
		// Then call the function
		return function(self, argv, argc);
	} else {
		// Otherwise, end the varargs
		va_end(args2);
		// Then call the function
		return function(self, NULL, 0);
	}
}
